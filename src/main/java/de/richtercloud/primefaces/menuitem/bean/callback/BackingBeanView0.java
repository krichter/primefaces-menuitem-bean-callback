package de.richtercloud.primefaces.menuitem.bean.callback;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DynamicMenuModel;
import org.primefaces.model.menu.MenuModel;

@Named
@ViewScoped
public class BackingBeanView0 implements Serializable {
    private static final long serialVersionUID = 1L;
    private MenuModel menuModel = new DynamicMenuModel();

    @PostConstruct
    private void init() {
        DefaultMenuItem menuItem = new DefaultMenuItem("Click me!",
                null, //icon
                "/index.xhtml" //url
        );
        menuItem.setCommand("#{backingBeanView0.onMenuItemClick('Hello world!')}");
        menuItem.setAjax(true);
        menuItem.setImmediate(true);
        menuModel.addElement(menuItem);
    }

    public void onMenuItemClick(String message) {
        System.out.println(BackingBeanView0.class.getName()+" message: "+message);
    }

    public MenuModel getMenuModel() {
        return menuModel;
    }

    public void setMenuModel(MenuModel menuModel) {
        this.menuModel = menuModel;
    }
}
