package de.richtercloud.primefaces.menuitem.bean.callback;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class BackingBeanSession0 implements Serializable {
    private static final long serialVersionUID = 1L;

    public void onMenuItemClick(String message) {
        System.out.println(BackingBeanSession0.class.getName()+" message: "+message);
    }
}
